<?php

namespace Gupo\PhpCrypto\Resources;

use Gupo\PhpCrypto\Enum\HttpResponseEnum;

class SuccessResource
{
    // 基础数据结构
    protected $response = [
        'code'    => HttpResponseEnum::SUCCESS,
        'message' => '请求成功',
        'data'    => []
    ];

    // 返回数据
    public function toResponse($data = [])
    {
        (is_array($data) || is_object($data)) && $data = json_decode(json_encode($data), true);

        if (is_array($data) && isset($data['data'])){
            $this->response = array_merge($this->response, $data);
        } else {
            $this->response['data'] = $data;
        }

        return $this->response;
    }
}
