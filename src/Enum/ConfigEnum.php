<?php

namespace Gupo\PhpCrypto\Enum;

class ConfigEnum
{
    /**
     * 长期有效配置
     */
    const PERMANENT_AT = '长期';

    /**
     * 不加密
     */
    const NOT_SAFETY = '0';

    /**
     * 加密
     */
    const IS_SAFETY = '1';

    /**
     * 密钥长度
     */
    const KEY_LENGHT = 16;

    /**
     * 十进制数
     */
    const DECIMALISM = 10;

    /**
     * 中位数
     */
    const MEDIAN = self::DECIMALISM / 2;

    /**
     * 正向
     */
    const AFT = 1;

    /**
     * 逆向
     */
    const BEF = -1;

    /**
     * 系数 1
     */
    const FACTOR_1 = 1;

    /**
     * 系数 2
     */
    const FACTOR_2 = 2;

    /**
     * 单机
     */
    const IS_SINGLE = 1;

    /**
     * 非单机
     */
    const NOT_SINGLE = 0;

    /**
     * 密钥格式 base64
     */
    const FORMAT_SIGN_BASE64 = 'base64';

    /**
     * 密钥格式 base64
     */
    const FORMAT_SIGN_HEX = 'hex';

    /**
     * 密钥是否使用中间椭圆 false
     */
    const RAND_FIXED_FALSE = false;

    /**
     * 密钥是否使用中间椭圆 true
     */
    const RAND_FIXED_TRUE = true;

    /**
     * 默认密钥有效时间 365天
     */
    const DEFAULT_EXPIRE_TIME = 365 * 24 * 60 * 60;

    /**
     * 密钥格式字典
     */
    const FORMAT_SIGN_MAP = [
        0 => self::FORMAT_SIGN_BASE64,
        1 => self::FORMAT_SIGN_HEX,
    ];

    /**
     * 密钥中间椭圆字典
     */
    const RAND_FIXED_MAP = [
        0 => self::RAND_FIXED_FALSE,
        1 => self::RAND_FIXED_TRUE,
    ];

    /**
     * 路由白名单
     */
    const ROUTE_WHITE_LIST = [
        'innerapi/*',
        'openapi/*',
        'api/gupo-crypto/crypto/*',
        'innerapi/gupo-crypto/crypto/*',
    ];

    /**
     * 加密状态码
     */
    const IS_ENCRYPT_CODE = 200;
}
