<?php

namespace Gupo\PhpCrypto\Enum;

class ErrorEnum
{
    /**
     * sm2加密失败
     */
    const SM2_ENCRYPT_FAIL = 'sm2 encrypt fail';

    /**
     * sm2解密失败
     */
    const SM2_DECRYPT_FAIL = 'sm2 decrypt fail';

    /**
     * sm4密钥长度错误
     */
    const SM4_KEY_LENGTH_FAIL = 'sm4 secret key length fail';

    /**
     * sm4解密失败
     */
    const SM4_ENCRYPT_FAIL = 'sm4 encrypt fail';

    /**
     * sm4加密失败
     */
    const SM4_DECRYPT_FAIL = 'sm4 decrypt fail';

    /**
     * 配置项不是数组
     */
    const CONFIG_NOT_AN_ARRAY = 'config empty or not an array';

    /**
     * 配置项不存在
     */
    const CONFIG_ITEM_NOT_EXIST = 'config item is null or not exist';

    /**
     * 配置项为空
     */
    const CONFIG_ITEM_EMPTY = 'config item is empty';

    /**
     * 密钥过期
     */
    const SECRET_KEY_EXPIRE = 'key expiration';

    /**
     * 配置缺失
     */
    const CONFIG_EMPTY = 'config is missing';

    /**
     * 密钥获取地址缺失
     */
    const SECRET_KEY_URL_EMPTY = 'key address is missing';

    /**
     * 密钥获取失败
     */
    const SECRET_KEY_REQUEST_FAIL = 'secret key request fail';

    /**
     * 配置文件不存在
     */
    const CONFIG_FILE_NOT_EXIST = 'config file not exist';

    /**
     * 密钥配置保存失败
     */
    const CONFIG_SAVE_FAIL = 'config save fail';

    /**
     * 资源名称
     */
    const PLATFORM_NAME_EMPTY = 'platform name is empty';
}
