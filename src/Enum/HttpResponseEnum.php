<?php

namespace Gupo\PhpCrypto\Enum;

class HttpResponseEnum
{

    // 成功
    const SUCCESS = 200;
    // 失败
    const ERROR = 400;
}
