<?php

namespace Gupo\PhpCrypto\Api;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Application AS LaravelApplication;

class CryptoRouteServiceProvider extends RouteServiceProvider
{

    /**
     * The path to the "home" route for your application.
     *
     * This is used by Laravel authentication to redirect users after login.
     *
     * @var string
     */
    public const HOME = '/home';

    /**
     * laravel 版本
     * @var string $frameworkVersion
     */
    protected $frameworkVersion;

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app instanceof LaravelApplication) {
            $this->frameworkVersion = $this->app->version();
            $arge_version = explode('.',$this->frameworkVersion)[0];
            switch ($arge_version){
                case '5':
                case '6':
                    parent::boot();
                    $this->mapGupoPhpCryptoRoutesVer6();
                    break;
                case '9':
                    $this->mapGupoPhpCryptoRoutesVer9();
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * larverl6.x 路由
     * @return void
     */
    protected function mapGupoPhpCryptoRoutesVer6()
    {
        /*国密包基础api*/
        Route::prefix('api')
            ->namespace($this->namespace)
            ->group(__DIR__ . '/Routes/laravel/api.php');
        /*国密包基础内部api*/
        Route::prefix('innerapi')
            ->namespace($this->namespace)
            ->group(__DIR__ . '/Routes/laravel/innerapi.php');
    }


    /**
     * larverl9.x 路由
     * @return void
     */
    protected function mapGupoPhpCryptoRoutesVer9()
    {
        $this->routes(function () {
            /*国密包基础api*/
            Route::prefix('api')
                ->namespace($this->namespace)
                ->group(__DIR__ . '/Routes/laravel/api.php');
            /*国密包基础内部api*/
            Route::prefix('innerapi')
                ->namespace($this->namespace)
                ->group(__DIR__ . '/Routes/laravel/innerapi.php');
        });
    }
}
