<?php

namespace Gupo\PhpCrypto\Api\Controllers;

use Gupo\PhpCrypto\Request\Api\CryptoRequest;
use Gupo\PhpCrypto\Resources\SuccessResource;
use Gupo\PhpCrypto\Crypto;
use Gupo\PhpCrypto\Sm\Sm2;

class CryptoController
{

    /**
     * 生成新的sm2公私钥
     * @return array|mixed
     */
    public function buildSm2Cipher()
    {
        return (new SuccessResource())->toResponse((new Sm2())->buildSm2Cipher());
    }

    /**
     * 获取公钥
     * @param Crypto $crypto
     * @return array|mixed
     */
    public function getPublicKey(Crypto $crypto)
    {
        return (new SuccessResource())->toResponse(['public_key' => $crypto->getPublicKey(), 'is_safety' => $crypto->getIsSafety()]);
    }

    /**
     * 密钥过期通知
     * @param Crypto $crypto
     * @return array|mixed
     */
    public function cipherExpireNotice(Crypto $crypto)
    {
        return (new SuccessResource())->toResponse(($crypto->clearCipherConfig() ? '密钥清理成功' : '密钥清理失败'));
    }

    /**
     * sm2加密
     * @param CryptoRequest $request
     * @param Crypto $crypto
     * @return array|mixed
     */
    public function sm2Encrypt(CryptoRequest $request, Crypto $crypto)
    {
        return (new SuccessResource())->toResponse($crypto->getEncryptTextBySm2($request->input('text')));
    }

    /**
     * sm2解密
     * @param CryptoRequest $request
     * @param Crypto $crypto
     * @return array|mixed
     */
    public function sm2Decrypt(CryptoRequest $request, Crypto $crypto)
    {
        return (new SuccessResource())->toResponse($crypto->getDecryptTextBySm2($request->input('text')));
    }

    /**
     * sm4加密
     * @param CryptoRequest $request
     * @param Crypto $crypto
     * @return array|mixed
     * @throws \Exception
     */
    public function sm4Encrypt(CryptoRequest $request, Crypto $crypto)
    {
        return (new SuccessResource())->toResponse($crypto->getEncryptTextBySm4($request->input('text')));
    }

    /**
     * sm4解密
     * @param CryptoRequest $request
     * @param Crypto $crypto
     * @return array|mixed
     * @throws \Exception
     */
    public function sm4Decrypt(CryptoRequest $request, Crypto $crypto)
    {
        return (new SuccessResource())->toResponse(json_decode($crypto->getDecryptTextBySm4($crypto->getSm4Secret($request->input('x-data-time')), $request->input('text')), true));
    }
}
