<?php

use Illuminate\Support\Facades\Route;

use Gupo\PhpCrypto\Api\Controllers\CryptoController;

Route::prefix('gupo-crypto')->group(function () {
    Route::prefix('crypto')->group(function (){
        Route::post('get-public-key', [ CryptoController::class, 'getPublicKey' ]); // 获取公钥
        Route::post('sm2-encrypt', [ CryptoController::class, 'sm2Encrypt' ]); // sm2加密
        Route::post('sm2-decrypt', [ CryptoController::class, 'sm2Decrypt' ]); // sm2解密
        Route::post('sm4-encrypt', [ CryptoController::class, 'sm4Encrypt' ]); // sm4加密
        Route::post('sm4-decrypt', [ CryptoController::class, 'sm4Decrypt' ]); // sm4解密
    });
});


Route::fallback(function () {
    return response()->json([
        'code'    => 404,
        'message' => 'Not Found!',
        'data'    => [],
    ]);
});
