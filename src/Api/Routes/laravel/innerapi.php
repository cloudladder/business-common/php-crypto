<?php

use Illuminate\Support\Facades\Route;

use Gupo\PhpCrypto\Api\Controllers\CryptoController;

Route::prefix('gupo-crypto')->group(function () {
    Route::prefix('crypto')->group(function (){
        Route::post('cipher-expire-notice', [ CryptoController::class, 'cipherExpireNotice' ]); // 密钥过期通知
    });
});


Route::fallback(function () {
    return response()->json([
        'code'    => 404,
        'message' => 'Not Found!',
        'data'    => [],
    ]);
});
