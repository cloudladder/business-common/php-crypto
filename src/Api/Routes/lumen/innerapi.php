<?php

$router->group(['prefix'=>'gupo-crypto/crypto'], function () use ($router){
    $router->post('cipher-expire-notice', 'CryptoController@cipherExpireNotice'); // 密钥过期通知
});
