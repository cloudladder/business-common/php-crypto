<?php

$router->group(['prefix'=>'gupo-crypto/crypto'], function () use ($router){
    $router->post('get-public-key', 'CryptoController@getPublicKey'); // 获取公钥
    $router->post('sm2-encrypt', 'CryptoController@sm2Encrypt'); // sm2加密
    $router->post('sm2-decrypt', 'CryptoController@sm2Decrypt'); // sm2解密
    $router->post('sm4-encrypt', 'CryptoController@sm4Encrypt'); // sm4加密
    $router->post('sm4-decrypt', 'CryptoController@sm4Decrypt'); // sm4解密
});
