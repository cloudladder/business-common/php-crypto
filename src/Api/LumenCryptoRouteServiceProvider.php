<?php

namespace Gupo\PhpCrypto\Api;

use Carbon\Laravel\ServiceProvider as RouteServiceProvider;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Laravel\Lumen\Application as LumenApplication;

class LumenCryptoRouteServiceProvider extends RouteServiceProvider
{

    /**
     * The path to the "home" route for your application.
     *
     * This is used by Laravel authentication to redirect users after login.
     *
     * @var string
     */
    public const HOME = '/home';

    /**
     * laravel 版本
     * @var string $frameworkVersion
     */
    protected $frameworkVersion;

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app instanceof LumenApplication) {
            preg_match("/Lumen [(\d\.\d\.\d)]*/", $this->app->version(), $this->frameworkVersion);
            $this->frameworkVersion = empty($this->frameworkVersion[0]) ? '' : explode('(', $this->frameworkVersion[0])[1];
            // 获取版本号
            $arge_version = explode('.', $this->frameworkVersion)[0];
            switch ($arge_version) {
                case '5':
                case '6':
                    parent::boot();
                    $this->mapGupoPhpCryptoRoutesLumen();
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * lumen 路由
     * @return void
     */
    protected function mapGupoPhpCryptoRoutesLumen()
    {
        /*国密包基础api*/
        Route::group([
            'prefix'    => 'api',
            'namespace' => 'Gupo\PhpCrypto\Api\Controllers'
        ], function ($router) {
            require __DIR__ . '/Routes/lumen/api.php';
        });
        /*国密包基础内部api*/
        Route::group([
            'prefix'    => 'innerapi',
            'namespace' => 'Gupo\PhpCrypto\Api\Controllers'
        ], function ($router) {
            require __DIR__ . '/Routes/lumen/innerapi.php';
        });
    }
}
