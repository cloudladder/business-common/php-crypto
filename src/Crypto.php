<?php

namespace Gupo\PhpCrypto;

use Illuminate\Support\Str;
use Gupo\PhpCrypto\Sm\Sm2;
use Gupo\PhpCrypto\Sm\Sm4;
use Gupo\PhpCrypto\Config\Config;

class Crypto extends Config
{

    /**
     * 版本号
     * @var string $version
     */
    protected $version = '0.1.60';

    /**
     * sm4加密算法
     */
    private const SM4_TYPE = 'sm4-ecb';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 生成非对称密钥
     * @param string $formatSign 仅支持 hex 和 base64
     * @return array
     */
    public function buildSm2Cipher()
    {
        return (new Sm2())->buildSm2Cipher();
    }

    /**
     * 清理密钥配置
     * @return bool
     */
    public function clearCipherConfig()
    {
        return $this->clearConfig();
    }

    /**
     * 获取公钥
     * @return string
     */
    public function getPublicKey()
    {
        return $this->publicKey ?? '';
    }

    /**
     * 获取加解密控制流程标签
     * @return string
     */
    public function getIsSafety()
    {
        return $this->isSafety ?? '1';
    }

    /**
     * 生成sm4密钥
     * @param string $time
     * @return string
     */
    public function getSm4Secret($time){
        return Sm4::buildSm4Secret($this->publicKey, $time);
    }

    /**
     * sm4加密
     * @param string $text
     * @return array
     * @throws \Exception
     */
    public function getEncryptTextBySm4(string $text = '')
    {
        $data_time = time();
        $random    = $this->getSm4Secret($data_time);
        $data      = (new Sm4($random))->sm4Encrypt($text, $this::SM4_TYPE);
        return ['key' => $random, 'data' => $data, 'data_time' => $data_time];
    }

    /**
     * sm4解密
     * @param string $key
     * @param string $text
     * @return string
     * @throws \Exception
     */
    public function getDecryptTextBySm4(string $key, string $text)
    {
        return (new Sm4($key))->sm4Decrypt($text, $this::SM4_TYPE);
    }

    /**
     * sm2加密
     * @param string $text
     * @return string
     */
    public function getEncryptTextBySm2(string $text)
    {
        return (new Sm2($this->formatSign))->sm2Encrypt($this->publicKey, $text);
    }

    /**
     * sm2解密
     * @param string $text
     * @return string
     */
    public function getDecryptTextBySm2(string $text)
    {
        return (new Sm2($this->formatSign))->sm2Decrypt($this->privateKey, $text);
    }

    /**
     * sm2和sm4组合加密
     * @param string $text
     * @return array
     * @throws \Exception
     */
    public function getEncryptText(string $text = '')
    {
        $data        = $this->getEncryptTextBySm4($text);
        $data['key'] = $this->getEncryptTextBySm2($data['key']);
        return $data;
    }

    /**
     * sm2和sm4组合解密
     * @param string $text
     * @param string $key
     * @return string
     * @throws \Exception
     */
    public function getDecryptText(string $text = '', string $key = '')
    {
        $key = $this->getDecryptTextBySm2($key);
        return $this->getDecryptTextBySm4(hex2bin($key), $text);
    }
}
