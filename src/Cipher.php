<?php

namespace Gupo\PhpCrypto;

use Gupo\PhpCrypto\Exception\CryptoException;
use Gupo\PhpCrypto\Sm\Sm2;
use Gupo\PhpCrypto\Sm\Sm4;

class Cipher
{

    /**
     * sm2私钥
     * @var $privateKey
     */
    protected $privateKey;

    /**
     * sm2公钥
     * @var $publicKey
     */
    protected $publicKey;

    /**
     * 输入输出的签名方式 16进制的还是base64
     * @var string
     */
    protected $formatSign;

    /**
     * 是否使用中间椭圆，使用中间椭圆的话，速度会快一些，但同样的数据的签名或加密的值就固定了
     * @var bool
     */
    protected $randFixed;

    /**
     * sm4加密算法
     */
    private const SM4_TYPE = 'sm4-ecb';

    /**
     * @param string $privateKey sm2私钥
     * @param string $publicKey sm2公钥
     * @param string $formatSign 仅支持 hex 和 base64
     * @param bool $randFixed 是否使用中间椭圆，使用中间椭圆的话，速度会快一些，但同样的数据的签名或加密的值就固定了，false 即不使用
     */
    public function __construct(string $privateKey, string $publicKey, string $formatSign = 'base64', bool $randFixed = false)
    {
        $this->privateKey = $privateKey;
        $this->publicKey  = $publicKey;
        $this->formatSign = $formatSign;
        $this->randFixed  = $randFixed;
    }

    /**
     * sm2加密
     * @param string $text
     * @return string
     * @throws CryptoException
     */
    public function getEncryptTextBySm2(string $text)
    {
        return (new Sm2($this->formatSign))->sm2Encrypt($this->publicKey, $text);
    }

    /**
     * sm2解密
     * @param string $text
     * @return string
     * @throws CryptoException
     */
    public function getDecryptTextBySm2(string $text)
    {
        return (new Sm2($this->formatSign))->sm2Decrypt($this->privateKey, $text);
    }

    /**
     * 生成sm4密钥
     * @param string $time
     * @return string
     */
    public function getSm4Secret($time)
    {
        return Sm4::buildSm4Secret($this->publicKey, $time);
    }

    /**
     * sm4加密
     * @param string $text
     * @return array
     * @throws \Exception
     */
    public function getEncryptTextBySm4(string $text = '')
    {
        $data_time = time();
        $random    = $this->getSm4Secret($data_time);
        $data      = (new Sm4($random))->sm4Encrypt($text, $this::SM4_TYPE);
        return ['key' => $random, 'data' => $data, 'data_time' => $data_time];
    }

    /**
     * sm4解密
     * @param string $key
     * @param string $text
     * @return string
     * @throws CryptoException
     */
    public function getDecryptTextBySm4(string $key, string $text)
    {
        return (new Sm4($key))->sm4Decrypt($text, $this::SM4_TYPE);
    }

    /**
     * sm2和sm4组合加密
     * @param string $text
     * @return array
     * @throws CryptoException
     */
    public function getEncryptText(string $text = '')
    {
        $data        = $this->getEncryptTextBySm4($text);
        $data['key'] = $this->getEncryptTextBySm2($data['key']);
        return $data;
    }

    /**
     * sm2和sm4组合解密
     * @param string $text
     * @param string $key
     * @return string
     * @throws CryptoException
     */
    public function getDecryptText(string $text = '', string $key = '')
    {
        $key = $this->getDecryptTextBySm2($key);
        return $this->getDecryptTextBySm4(hex2bin($key), $text);
    }
}