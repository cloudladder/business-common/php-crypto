<?php

namespace Gupo\PhpCrypto\Sm;

use Rtgm\sm\RtSm2;
use Gupo\PhpCrypto\Exception\CryptoException;
use Gupo\PhpCrypto\Enum\ErrorEnum;

class Sm2 extends RtSm2
{
    /**
     * 输入输出的签名方式 16进制的还是base64
     * @var string
     */
    protected string $format_sign;

    /**
     * 是否使用中间椭圆，使用中间椭圆的话，速度会快一些，但同样的数据的签名或加密的值就固定了
     * @var bool
     */
    protected bool $rand_fixed;

    /**
     * 初始化
     * @param string $formatSign
     * @param bool $randFixed 是否使用中间椭圆，使用中间椭圆的话，速度会快一些，但同样的数据的签名或加密的值就固定了
     */
    public function __construct(string $formatSign = 'base64', bool $randFixed = false)
    {
        parent::__construct($formatSign, $randFixed);

        $this->format_sign = $formatSign;
        $this->rand_fixed  = $randFixed;
    }

    /**
     * 生成非对称密钥
     * @return array
     */
    public function buildSm2Cipher()
    {
        list($privateKey, $publicKey) = $this->generatekey();
        return ['privateKey' => $privateKey, 'publicKey' => $publicKey, 'formatSign' => $this->format_sign, 'randFixed' => $this->rand_fixed];
    }

    /**
     * SM2 加密
     * @param string $publicKey
     * @param string $text
     * @return string
     */
    public function sm2Encrypt(string $publicKey, string $text = '')
    {
        $data = $this->doEncrypt($text, $publicKey);
        if (empty($data)) throw new CryptoException(ErrorEnum::SM2_ENCRYPT_FAIL);
        return base64_encode($data);
    }

    /**
     * SM2 解密
     * @param string $privateKey
     * @param string $text
     * @return string
     */
    public function sm2Decrypt(string $privateKey, string $text = '')
    {
        $data = $this->doDecrypt(base64_decode($text), $privateKey);
        if (empty($data)) throw new CryptoException(ErrorEnum::SM2_DECRYPT_FAIL);
        return $data;
    }
}
