<?php

namespace Gupo\PhpCrypto\Sm;

use Gupo\PhpCrypto\Enum\ConfigEnum;
use Rtgm\sm\RtSm4;
use Gupo\PhpCrypto\Exception\CryptoException;
use Gupo\PhpCrypto\Enum\ErrorEnum;

class Sm4 extends RtSm4
{

    /**
     * @param string $key 必须是16进制数
     * @throws CryptoException
     */
    public function __construct($key)
    {
        if (strlen($key) !== $this->ivLen) throw new CryptoException(ErrorEnum::SM4_KEY_LENGTH_FAIL);
        parent::__construct($key);
    }

    /**
     * SM4 加密
     * @param string $text
     * @param string $type
     * @return string
     * @throws CryptoException
     */
    public function sm4Encrypt(string $text = '', string $type = 'sm4-ecb'): string
    {
        $data = $this->encrypt($text, strtolower($type));
        if (empty($data)) throw new CryptoException(ErrorEnum::SM4_ENCRYPT_FAIL);
        return $data;
    }

    /**
     * SM4 解密
     * @param string $text
     * @param string $type
     * @return string
     * @throws CryptoException
     */
    public function sm4Decrypt(string $text = '', string $type = 'sm4-ecb'): string
    {
        $data = $this->decrypt($text, strtolower($type));
        if (empty($data)) throw new CryptoException(ErrorEnum::SM4_DECRYPT_FAIL);
        return $data;
    }

    /**
     * 生成随机串
     * @param int $length
     * @return string
     * @throws \Exception
     */
    public static function sm4Random($length = 16)
    {
        $string = '';

        while (($len = strlen($string)) < $length) {
            $size = $length - $len;

            $bytes = random_bytes($size);

            $string .= substr(str_replace(['/', '+', '='], '', base64_encode($bytes)), 0, $size);
        }

        return $string;
    }

    /**
     * 生成sm4密钥
     * @param string|integer $const_key 公钥
     * @param string|integer $salt 时间戳
     * @return string
     */
    public static function buildSm4Secret($const_key, $salt)
    {
        $method = substr($salt, -2, 1) > ConfigEnum::MEDIAN ? ConfigEnum::AFT : ConfigEnum::BEF; // 方向
        $offset = (int) substr($salt, -1, 1);
        $factor = abs($offset - $method) > ConfigEnum::MEDIAN ? ConfigEnum::FACTOR_2 : ConfigEnum::FACTOR_1; // 系数
        $offset = $offset * $factor * $method; // 偏移量

        $string = strtolower(md5($const_key . $salt));
        $key = substr($string, $offset, ConfigEnum::KEY_LENGHT);
        while (ConfigEnum::KEY_LENGHT > strlen($key)){
            $key .= substr($string, 0, ConfigEnum::KEY_LENGHT - strlen($key));
        }
        return $key;
    }
}
