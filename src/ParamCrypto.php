<?php

namespace Gupo\PhpCrypto;

use Closure;
use Illuminate\Http\Request;
use Gupo\PhpCrypto\Exception\CryptoException;
use Gupo\PhpCrypto\Enum\ErrorEnum;
use Gupo\PhpCrypto\Enum\ConfigEnum;
use Illuminate\Foundation\Application as LaravelApplication;
use Laravel\Lumen\Application as LumenApplication;

class ParamCrypto
{

    /**
     * 加解流程控制
     * @var string $is_safety
     */
    protected $is_safety = 'X-Is-Safety';

    /**
     * 加密密钥时间戳
     * @var string $date_time
     */
    protected $date_time = 'X-Data-Time';

    /**
     * 密钥
     * @var string $date_key
     */
    protected $date_key = 'X-Data-Key';

    /**
     * 可访问额外返回头
     * @var string $expose_headers
     */
    protected $expose_headers = 'Access-Control-Expose-Headers';

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse) $next
     *
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle($request, Closure $next)
    {
        $is_safety = $request->header($this->is_safety);
        if (ConfigEnum::NOT_SAFETY !== $is_safety) {
            $params = [];
            // 解密 query string 参数包
            $params = array_merge($params, $this->decryptParams($request->query('data'), $request->query('key'), 'GET'));
            // 解密 body 参数包
            $params = array_merge($params, $this->decryptParams($request->post('data'), $request->post('key'), 'POST'));
            // 合并回 request 参数列表
            $params && $request->replace($params);
        }
        $response = $next($request);

        $is_safety = $this->isSafetyResponse('/' . $request->path()) ? ConfigEnum::IS_SAFETY : ConfigEnum::NOT_SAFETY;
        header("{$this->expose_headers}:{$this->is_safety}, {$this->date_time}");
        header("{$this->is_safety}:{$is_safety}");
        header("{$this->date_time}:");
        if (ConfigEnum::NOT_SAFETY !== $is_safety) {
            if (method_exists($response, 'getData')) {
                $data = $response->getData(true);
            } else {
                $data = json_decode($response->getContent(), true);
            }
            if ((isset($data['code']) && $data['code'] == ConfigEnum::IS_ENCRYPT_CODE) || (!isset($data['code']) && isset($data['data']))) {
                // 以加密形式返回
                $sm4_data = (new Crypto())->getEncryptTextBySm4(json_encode($data['data'] ?? [], JSON_UNESCAPED_UNICODE));
                header("{$this->date_time}:{$sm4_data['data_time']}");
                $data['data'] = $sm4_data['data'];
                if (method_exists($response, 'setData')) {
                    $response->setData($data);
                } else {
                    $response->setContent(json_encode($data));
                }
            } else {
                // 碰到不规范接口或异常报错以非加密形式返回
                header("{$this->is_safety}:" . ConfigEnum::NOT_SAFETY);
            }
        }

        return $response;
    }

    /**
     * 参数包解密
     * @param string $data
     * @param string $key
     * @param string $method
     * @return array|mixed
     * @throws CryptoException
     */
    private function decryptParams($data, $key, $method)
    {
        if (empty($data) || empty($key)) return [];
        $params = (new Crypto())->getDecryptText($data, $key);
        if (empty($params)) throw new CryptoException(ErrorEnum::SM4_DECRYPT_FAIL);

        // json > array
        $arg = json_decode($params, true);
        // query string > array
        if (!is_array($arg)) {
            parse_str($params, $params);
        } else {
            $params = $arg;
        }
        $isLog = config('cryptoConfig.decrypt_log', false);
        $isLog && $this->writeLog($params, $method);
        return empty($params) ? [] : $params;
    }

    /**
     * 是否加密返回
     * false: 不加密返回, true: 加密返回
     * @param string $api_path
     * @return string
     */
    private function isSafetyResponse($api_path)
    {
        $is_white_route = $this->isWhiteRoute($api_path); # 判断是否为白名单路由，白名单路由明文返回数据
        if ($is_white_route) {
            return ConfigEnum::NOT_SAFETY;
        } elseif ((new Crypto())->getIsSafety() === ConfigEnum::NOT_SAFETY) {
            return ConfigEnum::NOT_SAFETY;
        } else {
            return ConfigEnum::IS_SAFETY;
        }

    }

    /**
     * 判断是否白名单路由
     * @param string $route_name
     * @return bool
     */
    private function isWhiteRoute($route_name)
    {
        $routes           = explode('/', $route_name);
        $route_white_list = config('cryptoConfig.white');
        !is_array($route_white_list) && $route_white_list = explode(',', $route_white_list);
        $route_white_list = empty($route_white_list[0]) ? ConfigEnum::ROUTE_WHITE_LIST : array_merge(ConfigEnum::ROUTE_WHITE_LIST, $route_white_list);
        array_walk($route_white_list, function (&$v, $i) {
            $v = '/' . trim($v, '/');
        });

        $is_white = false;
        $route    = [];
        $max_key  = max(array_keys($routes));
        if (count($routes) > 0) {
            array_walk($routes, function ($v, $k) use (&$is_white, &$route, $route_white_list, $max_key) {
                if (!$is_white) {
                    $route[]   = $v;
                    $tmp_route = implode('/', $route) . ($max_key == $k ? '' : '/*');
                    $is_white  = in_array($tmp_route, $route_white_list);
                }
            });
        } else {
            $is_white = in_array($route_name, $route_white_list);
        }

        return $is_white;
    }

    /**
     * 写日志
     * @param array $args
     * @param string $method
     * @return void
     */
    private function writeLog($args, $method)
    {
        $logName = 'crypto request';
        if (app() instanceof LaravelApplication) {
            \Log::channel('debug')->debug("{$logName} method:{$method} params:" . json_encode($args, JSON_UNESCAPED_UNICODE));
        } else if (app() instanceof LumenApplication){
            \Log::channel('ase')->debug("{$logName} method:{$method} params:" . json_encode($args, JSON_UNESCAPED_UNICODE));
        }
    }
}
