<?php

namespace Gupo\PhpCrypto\Utils;

class Utils
{
    /**
     * Check the string is empty?
     *
     * @param  string  $val
     * @return bool if string is null or zero length, return true
     */
    public static function empty_($val)
    {
        return empty($val);
    }

    /**
     * Check one value is unset.
     *
     * @param  mixed  $value
     * @return bool if unset, return true
     */
    public static function isUnset(&$value = null)
    {
        return !isset($value) || null === $value;
    }

    /**
     * Assert a value, if it is a string, return it, otherwise throws.
     *
     * @param  mixed  $value
     * @return string the string value
     */
    public static function assertAsString($value)
    {
        if (\is_string($value)) {
            return $value;
        }

        throw new \InvalidArgumentException('It is not a string value.');
    }

    /**
     * Assert a value, if it is a number, return it, otherwise throws.
     *
     * @param  mixed  $value
     * @return bool the number value
     */
    public static function assertAsNumber($value)
    {
        if (\is_numeric($value)) {
            return $value;
        }

        throw new \InvalidArgumentException('It is not a number value.');
    }

    /**
     * 断言为数组
     *
     * @param $any
     * @return array
     * @author Wumeng wumeng@gupo.onaliyun.com
     * @since 2023-07-03 17:55
     */
    public static function assertAsArray($any)
    {
        if (\is_array($any)) {
            return $any;
        }

        throw new \InvalidArgumentException('It is not a array value.');
    }
}
