<?php

namespace Gupo\PhpCrypto\Support\Kernel;

use Gupo\PhpCrypto\Enum\HttpResponseEnum;
use Illuminate\Http\JsonResponse;

class HttpResponder
{

    /**
     * 默认响应信息
     */
    public const NORMAL_MESSAGE = 'success';

    /**
     * 成功响应信息
     */
    public const SUCCESS_MESSAGE = '操作成功';

    /**
     * 失败响应信息
     */
    public const ERROR_MESSAGE = '操作失败';

    /**
     * 响应
     * @param string $message 响应信息
     * @param int $code 响应码
     * @param mixed $data 响应数据
     * @return JsonResponse
     */
    protected static function toResponse(string $message = self::NORMAL_MESSAGE, int $code = HttpResponseEnum::SUCCESS, $data = []): JsonResponse
    {
        return new JsonResponse([
            'message' => $message,
            'code' => $code,
            'data' => $data,
        ]);
    }

    /**
     * 响应成功信息
     * @param string $message 成功信息
     * @param mixed $data 响应数据
     * @param int $code 响应码（默认1000）
     * @return JsonResponse
     */
    public static function responseSuccess(string $message = self::SUCCESS_MESSAGE, $data = [], int $code = HttpResponseEnum::SUCCESS): JsonResponse
    {
        return self::toResponse($message, $code, $data);
    }

    /**
     * 响应失败信息
     * @param string $message 错误信息
     * @param int $code 响应码（默认403）
     * @return JsonResponse
     */
    public static function responseError(string $message = self::ERROR_MESSAGE, int $code = HttpResponseEnum::ERROR): JsonResponse
    {
        return self::toResponse($message, $code);
    }
}
