<?php

namespace Gupo\PhpCrypto\Config;

use Cloudladder\Http\Client;
use Gupo\PhpCrypto\Enum\ConfigEnum;
use Gupo\PhpCrypto\Enum\ErrorEnum;
use Gupo\PhpCrypto\Sm\Sm2;
use Gupo\PhpCrypto\Utils\Utils;
use Gupo\PhpCrypto\Exception\CryptoException;

class Config
{
    /**
     * 公钥
     * @var string $publicKey
     */
    public $publicKey;

    /**
     * 私钥
     * @var string $privateKey
     */
    public $privateKey;

    /**
     * SM2 输出签名方式
     * @var string $formatSign
     */
    public $formatSign;

    /**
     * SM2 是否使用中间椭圆，使用中间椭圆的话，速度会快一些，但同样的数据的签名或加密的值就固定了
     * @var bool $randFixed
     */
    public $randFixed;

    /**
     * 公私钥到期时间，精确到 年月日 时分秒
     * @var string $expireTime
     */
    public $expireTime;

    /**
     * 加解流程控制
     * @var string $isSafety
     */
    public $isSafety;

    /**
     * config 文件路径
     * @var string $configPath
     */
    public $configPath = __DIR__ . '/crypto.php';

    /**
     * 外部配置
     * @var array $outerConfig
     */
    public $outerConfig;

    /**
     * @throws CryptoException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function __construct()
    {
        $this->init();
    }

    /**
     * 初始化
     * @return void
     * @throws CryptoException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function init()
    {
        $config = [];
        if (file_exists($this->configPath)) {
            $config = include $this->configPath;
            (empty($config) || !is_array($config)) && $config = $this->saveConfig();
        }

        if (!$this->checkConfigIntact($config) || !$this->checkConfigForExpire($config['expire_time'])) {
            $config = $this->saveConfig();
            if (!$this->checkConfigIntact($config)) throw new CryptoException(ErrorEnum::CONFIG_ITEM_NOT_EXIST);
            if (!$this->checkConfigForExpire($config['expire_time'])) throw new CryptoException(ErrorEnum::SECRET_KEY_EXPIRE);
        }

        $this->publicKey  = $config['public_key'] ?? '';
        $this->privateKey = $config['private_key'] ?? '';
        $this->formatSign = $config['format_sign'];
        $this->randFixed  = $config['rand_fixed'];
        $this->expireTime = $config['expire_time'] ?? '';
        $this->isSafety   = $config['is_safety'];
    }

    /**
     * 保存配置
     * @return array
     * @throws CryptoException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function saveConfig()
    {
        $config = $this->getConfigByConfigCenter();
        !isset($config['format_sign']) && $config['format_sign'] = ConfigEnum::FORMAT_SIGN_BASE64;
        !isset($config['rand_fixed']) && $config['rand_fixed'] = ConfigEnum::RAND_FIXED_FALSE;
        !isset($config['is_safety']) && $config['is_safety'] = ConfigEnum::IS_SAFETY;
        if (empty($config['private_key'])
            || empty($config['public_key'])
            || empty($config['expire_time'])
        ) {
            return $config;
        }

        $res = file_put_contents($this->configPath, "<?php return unserialize('" . serialize($config) . "');");
        if ($res === false) throw new CryptoException(ErrorEnum::CONFIG_SAVE_FAIL);
        return $config;
    }

    /**
     * 获取配置
     * @return array
     * @throws CryptoException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getConfigByConfigCenter()
    {
        $outerConfigPath = rtrim(__DIR__, '/') . '/../../../../../config/cryptoConfig.php';
//        $outerConfigPath = rtrim($_SERVER['PWD'], '/') . '/config/cryptoConfig.php';
        if (!file_exists($outerConfigPath)) throw new CryptoException(ErrorEnum::CONFIG_FILE_NOT_EXIST);
        $this->outerConfig = include $outerConfigPath;
        if (empty($this->outerConfig)) throw new CryptoException(ErrorEnum::CONFIG_EMPTY);
        $is_single = $this->outerConfig['is_single'] ?? 0;
        switch ($is_single) {
            case ConfigEnum::IS_SINGLE:
                $config = $this->single();
                break;
            default:
                $config = $this->notSingle();
                break;
        }
        return $config;
    }

    /**
     * 清理密钥配置
     * @return bool
     */
    public function clearConfig()
    {
        if (!unlink($this->configPath)) {
            $res = file_put_contents($this->configPath, "<?php return '';");
            if ($res === false) return false;
        }
        return true;
    }

    /**
     * 单机模式生成密钥配置
     * @return array
     */
    private function single()
    {
        $outerConfig = $this->outerConfig;
        $formatSign  = empty($outerConfig['format_sign']) || !isset(ConfigEnum::FORMAT_SIGN_MAP[$outerConfig['format_sign']]) ? ConfigEnum::FORMAT_SIGN_BASE64 : ConfigEnum::FORMAT_SIGN_MAP[$outerConfig['format_sign']];
        $randFixed   = empty($outerConfig['rand_fixed']) || !isset(ConfigEnum::RAND_FIXED_MAP[$outerConfig['rand_fixed']]) ? ConfigEnum::RAND_FIXED_FALSE : ConfigEnum::RAND_FIXED_MAP[$outerConfig['rand_fixed']];
        $sm2_cipher  = (new Sm2($formatSign, $randFixed))->buildSm2Cipher();

        return [
            'public_key'  => $sm2_cipher['publicKey'] ?? '',
            'private_key' => $sm2_cipher['privateKey'] ?? '',
            'format_sign' => $sm2_cipher['formatSign'] ?? '',
            'rand_fixed'  => $sm2_cipher['randFixed'] ?? '',
            'expire_time' => date('Y-m-d H:i:s', time() + ConfigEnum::DEFAULT_EXPIRE_TIME),
            'is_safety'   => ConfigEnum::IS_SAFETY,
        ];
    }

    /**
     * 通过用户中心获取配置
     * @return array
     * @throws CryptoException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function notSingle()
    {
        $outerConfig = $this->outerConfig;
        if (empty($outerConfig['authorize_url'])) throw new CryptoException(ErrorEnum::SECRET_KEY_URL_EMPTY);
        if (empty($outerConfig['platform_name'])) throw new CryptoException(ErrorEnum::PLATFORM_NAME_EMPTY);
        $url            = rtrim($outerConfig['authorize_url'], '/') . '/' . ltrim((empty($outerConfig['api_name']) ? '/api/v2/system/cipher/sm2' : $outerConfig['api_name']), '/');
        $method         = strtolower(empty($outerConfig['method']) ? 'get' : $outerConfig['method']);
        $allow_code     = empty($outerConfig['allow_code']) ? [200] : explode(',', $outerConfig['allow_code']);
        $code_field     = empty($outerConfig['code_field']) ? 'code' : $outerConfig['code_field'];
        $result_field   = empty($outerConfig['result_field']) ? 'data' : $outerConfig['result_field'];
        $platform_field = empty($outerConfig['param_platform_field']) ? 'system_code' : $outerConfig['param_platform_field'];

        $post_data = ['json' => [$platform_field => $outerConfig['platform_name']]];
        if ($method == 'post') {
            $ret = (new Client())->post($url, $post_data);
        } else {
            $ret = (new Client())->get($url, $post_data);
        }
        $ret = json_decode($ret->getBody()->getContents(), true);
        if (!isset($ret[$code_field]) || !in_array($ret[$code_field], $allow_code)) throw new CryptoException(ErrorEnum::SECRET_KEY_REQUEST_FAIL);

        $public_key_field  = empty($outerConfig['ret_public_key_field']) ? 'key_value.public_key' : $outerConfig['ret_public_key_field'];
        $private_key_field = empty($outerConfig['ret_private_key_field']) ? 'key_value.private_key' : $outerConfig['ret_private_key_field'];
        $format_sign_field = empty($outerConfig['ret_format_sign_field']) ? 'key_value.format_sign' : $outerConfig['ret_format_sign_field'];
        $rand_fixed_field  = empty($outerConfig['ret_rand_fixed_field']) ? 'key_value.rand_fixed' : $outerConfig['ret_rand_fixed_field'];
        $expire_time_field = empty($outerConfig['ret_expire_time_field']) ? 'key_value.expire_time' : $outerConfig['ret_expire_time_field'];
        $is_safety_field   = empty($outerConfig['ret_is_safety_field']) ? 'key_value.is_safety' : $outerConfig['ret_is_safety_field'];

        // 加解流程控制
        $is_safety = (string) $this->getResultParamValue($ret[$result_field], $is_safety_field);
        // 到期时间
        $expire_time = $this->getResultParamValue($ret[$result_field], $expire_time_field);
        return [
            'public_key'  => $this->getResultParamValue($ret[$result_field], $public_key_field),
            'private_key' => $this->getResultParamValue($ret[$result_field], $private_key_field),
            'format_sign' => $this->getResultParamValue($ret[$result_field], $format_sign_field),
            'rand_fixed'  => $this->getResultParamValue($ret[$result_field], $rand_fixed_field),
            'expire_time' => is_null($expire_time) ? '' : ($expire_time ? $expire_time : ConfigEnum::PERMANENT_AT),  // 到期时间为空字符串, 即长期有效
            'is_safety'   => ConfigEnum::NOT_SAFETY !== $is_safety ? ConfigEnum::IS_SAFETY : ConfigEnum::NOT_SAFETY, // 加解流程控制; 0: 关闭加解密, 1:开启加解密
        ];
    }

    /**
     * 检查配置完整性
     * false: 缺失, true: 完整
     * @param array $config
     * @return bool
     */
    private function checkConfigIntact($config)
    {
        if (Utils::isUnset($config['private_key'])
            || Utils::isUnset($config['public_key'])
            || Utils::isUnset($config['format_sign'])
            || Utils::isUnset($config['rand_fixed'])
            || Utils::isUnset($config['expire_time'])
            || Utils::isUnset($config['is_safety'])
        ) {
            return false;
        }
        return true;
    }

    /**
     * 检查配置是否过期
     * false: 已过期, true: 未过期
     * @param string $expire_time
     * @return bool
     */
    private function checkConfigForExpire($expire_time)
    {
        if (ConfigEnum::PERMANENT_AT !== $expire_time && time() > strtotime($expire_time)) return false;
        return true;
    }

    /**
     * 提取所需参数
     * @param array $result_arr
     * @param string $param_field
     * @return mixed|string|null
     */
    private function getResultParamValue($result_arr, $param_field)
    {
        $param_field = explode('.', $param_field);
        foreach ($param_field as $key => $val) {
            if (!isset($result_arr[$val])) return null;
            if (!is_array($result_arr[$val])) {
                return $result_arr[$val] ?? '';
            }
            $result_arr = $result_arr[$val];
        }
        return null;
    }
}
