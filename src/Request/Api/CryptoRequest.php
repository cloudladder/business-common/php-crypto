<?php

namespace Gupo\PhpCrypto\Request\Api;

use Gupo\PhpCrypto\Request\Support\BaseRequest;
use Illuminate\Support\Str;

class CryptoRequest extends BaseRequest
{
    public function rules()
    {
        $action_name = Str::after($this->route()->getActionName(), '@');
        switch ($action_name) {
            case 'sm2Encrypt':
            case 'sm2Decrypt':
            case 'sm4Encrypt':
                $return = [
                    'text' => 'bail|required|string',
                ];
                break;
            case 'sm4Decrypt':
                $return = [
                    'x-data-time' => 'bail|required|string',
                    'text'        => 'bail|required|string',
                ];
                break;
            default:
                $return = [];
                break;
        }

        return $return;
    }

    public function messages()
    {
        return [
            'required'             => ':attribute 缺少必要参数',
            'text.required'        => ':attribute 原文或密文不允许为空',
            'text.string'          => ':attribute 原文或密文必须是字符串',
            'x-data-time.required' => ':attribute 加密时间戳不允许为空',
            'x-data-time.string'   => ':attribute 加密时间戳必须是字符串',
        ];
    }
}
