<?php

namespace Gupo\PhpCrypto\Request\Support;

use Illuminate\Foundation\Http\FormRequest;
use Gupo\PhpCrypto\Request\Traits\RequestDefaultAuthorize;
use Gupo\PhpCrypto\Request\Traits\RequestFailedValidation;
use Gupo\PhpCrypto\Request\Traits\RequestScenes;
use Gupo\PhpCrypto\Request\Traits\RunValidator;

abstract class BaseRequest extends FormRequest
{
    use RequestDefaultAuthorize, RequestFailedValidation, RequestScenes, RunValidator;
    /**
     * 表示验证器是否应在第一个规则失败时停止。
     *
     * @var bool
     */
    protected $stopOnFirstFailure = true;
}
