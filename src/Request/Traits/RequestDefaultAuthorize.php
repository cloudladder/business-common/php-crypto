<?php

namespace Gupo\PhpCrypto\Request\Traits;

/**
 * 验证器默认鉴权
 */
trait RequestDefaultAuthorize
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
