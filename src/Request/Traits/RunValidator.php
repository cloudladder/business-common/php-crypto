<?php

namespace Gupo\PhpCrypto\Request\Traits;

use Gupo\PhpCrypto\Exception\ValidationException;
use Illuminate\Support\Facades\Validator;

/**
 * 执行验证器
 */
trait RunValidator
{
    /**
     * 执行验证器
     *
     * @param mixed $data 待验证的数据
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public static function make($data = []): \Illuminate\Contracts\Validation\Validator
    {
        $instance = new static;

        return Validator::make((array) $data, $instance->rules(), $instance->messages(), $instance->attributes())
            ->stopOnFirstFailure($instance->stopOnFirstFailure);
    }

    /**
     * 执行验证器（错误时抛出异常）
     *
     * @param array $data 待验证的数据
     *
     * @return bool
     *
     * @throws \Exception
     */
    public static function checkOrFail($data = []): bool
    {
        $validator = self::make($data);
        if ($validator->fails()) {
            throw new ValidationException($validator->errors()->first());
        }

        return true;
    }

    /**
     * 执行集合验证器（错误时抛出异常）
     *
     * @param array|\Illuminate\Support\Collection $collection 待验证的数据集
     *
     * @return bool
     *
     * @throws \Exception
     */
    public static function checkCollectionOrFail($collection): bool
    {
        foreach ($collection as $item) {
            self::checkOrFail($item);
        }

        return true;
    }
}
