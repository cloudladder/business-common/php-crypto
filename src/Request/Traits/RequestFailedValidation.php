<?php

namespace Gupo\PhpCrypto\Request\Traits;

use Gupo\PhpCrypto\Exception\ValidationException;
use Illuminate\Contracts\Validation\Validator;

/**
 * 验证器自定义异常类
 */
trait RequestFailedValidation
{
    /**
     * @param Validator $validator
     * @return mixed
     * @throws ValidationException
     */
    public function failedValidation(Validator $validator)
    {
        throw new ValidationException($validator->errors()->first());
    }
}
