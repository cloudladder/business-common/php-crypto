<?php

namespace Gupo\PhpCrypto\Request\Traits;

use Illuminate\Support\Str;
use Illuminate\Validation\Validator;

/**
 * 验证器场景
 */
trait RequestScenes
{
    /**
     * 当前验证场景
     *
     * @var string
     */
    protected $currentScene = '';

    /**
     * 验证场景
     *
     * @return array
     */
    public function scenes(): array
    {
        return [];
    }

    /**
     * 指定当前验证场景
     *
     * @param string $currentScene
     *
     * @return self
     */
    public function useScene(string $currentScene): self
    {
        $this->currentScene = $currentScene;

        return $this;
    }

    /**
     * 配置验证实例
     *
     * @param Validator $validator
     *
     * @return void
     */
    public function withValidator(Validator $validator): void
    {
        $scenes = [];
        if (method_exists($this, 'scenes')) {
            $scenes = $this->container->call([$this, 'scenes']);
        }

        if (empty($scenes)) {
            return;
        }

        /** @var Validator $this */
        $rules = $this->container->call([$this, 'rules']);

        // 找到场景及对应的验证规则
        $currentScene = $this->currentScene ?: Str::after(request()->route()->getActionName(), '@');
        $sceneRules = $scenes[$currentScene] ?? null;
        if (is_null($sceneRules)) {
            return;
        }

        // 构建出场景相对应的验证规则集
        $resRules = [];
        foreach ($sceneRules as $key => $rule) {
            if (is_numeric($key) && !empty($ruleValue = $rules[$rule] ?? null)) {
                $resRules[$rule] = $ruleValue;
            } else {
                $resRules[$key] = $rule;
            }
        }
        $validator->setRules($resRules);
    }
}
