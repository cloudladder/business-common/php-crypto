<?php

namespace Gupo\PhpCrypto\Exception;

use Exception;

class CryptoException extends Exception
{
    /**
     * ClientException constructor
     *
     * @param $errorMessage
     * @param int $errorCode
     * @param $previous
     */
    public function __construct($errorMessage, $errorCode = 4222, $previous = null)
    {
        parent::__construct($errorMessage, $errorCode, $previous);
    }
}
