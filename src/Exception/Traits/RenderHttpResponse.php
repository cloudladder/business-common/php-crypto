<?php

namespace Gupo\PhpCrypto\Exception\Traits;

use Gupo\PhpCrypto\Support\Kernel\HttpResponder;

/**
 * 异常以接口的标准格式响应
 */
trait RenderHttpResponse
{
    public function render($request)
    {
        return HttpResponder::responseError($this->getMessage());
    }
}
