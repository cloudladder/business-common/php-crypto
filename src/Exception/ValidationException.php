<?php

namespace Gupo\PhpCrypto\Exception;

use Gupo\PhpCrypto\Exception\Traits\RenderHttpResponse;

class ValidationException extends \Exception implements \Throwable
{
    use RenderHttpResponse;
}
